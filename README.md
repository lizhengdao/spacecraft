# SpaceCraft

Basically a Minecraft ripoff with spherical planets instead of an infinite
plane. It was originally an experiment but I ended up adding some random
features to make it more like a game. I'm aiming for something like a
Minecraft/Scrap Mechanic hybrid. The future of this project is largely uncertain
though since I'm developing it only for fun.
Let me know if you are interested in the concept and I might invest more time
in it.

Built using the Unity Engine. If you want to try it out, just install
the editor, load the project and hit Play.