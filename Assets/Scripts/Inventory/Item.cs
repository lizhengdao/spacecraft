﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    None, Block, Voxel, DestroyTool, DiggingTool, Weapon, Component
}

public class Item : IDisposable
{
    public readonly int id;
    public string name;
    public ItemType type;
    public int stackCount = 1;
    public int maxStack = 16;
    // additional info (like durability or voxel type)
    public int value;

    private Tool tool;

    public Item(int _id, string _name, ItemType _type)
    {
        id = _id;
        name = _name;
        type = _type;
    }

    public Item Clone()
    {
        var item = (Item)MemberwiseClone();
        item.tool = null; //Give the new item a new tool
        return item;
    }

    public Sprite GetSprite()
    {
        if (type == ItemType.Voxel)
            return Planet.GetTile(value);
        else
            return ResourceBank.resources.items[id].sprite;
    }

    public Tool GetTool()
    {
        if (tool == null)
        {
            switch (type)
            {
                case ItemType.Block:
                    tool = new PlaceBlockTool(this);
                    break;
                case ItemType.Voxel:
                    tool = new VoxelTool(this);
                    break;
                case ItemType.DestroyTool:
                    tool = new DestroyTool(/*id*/);
                    break;
                case ItemType.DiggingTool:
                    tool = new DiggingTool(id);
                    break;
                case ItemType.Weapon:
                    tool = null; //new Weapon(id)
                    break;
            }
        }
        return tool;
    }

    public string GetName()
    {
        if (id == 9) //voxel
            return ResourceBank.voxelNames[value];
        else
            return name;
    }

    public string Serialize()
    {
        return id + ":" + value + "x" + stackCount;
    }

    public bool Matches(Item other)
    {
        return (id == other.id) && (value == other.value);
    }

    public bool CanAdd(Item other)
    {
        return Matches(other) && (stackCount < maxStack);
    }

    public static Item Deserialize(string str)
    {
        if (string.IsNullOrEmpty(str) || str == "e")
            return null;
        string[] str1 = str.Split('x');
        string[] str2 = str1[0].Split(':');
        int itemID = int.Parse(str2[0]);
        int value = str2.Length > 1 ? int.Parse(str2[1]) : 0;
        int count = str1.Length > 1 ? int.Parse(str1[1]) : 0;
        var item = ResourceBank.GetItem(itemID);
        item.value = value;
        item.stackCount = count;
        return item;
    }

    public void Dispose()
    {
        if (tool != null)
            tool.Deactivate();
    }
}
