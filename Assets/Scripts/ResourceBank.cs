﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResourceBank : MonoBehaviour
{
    //A singleton. Could have named it differently, but it just looks good to write it this way
    public static ResourceBank resources;

    public ItemData[] items = new ItemData[0];
    public GameObject[] screens;
    public Planet[] planetTypes;
    public ExtraDropItem[] extraDropItems;
    public GameObject ship;
    public GameObject chunk;
    public GameObject planet;
    public GameObject item;
    public GameObject button;
    public GameObject tooltip;
    public Texture2D tileTexture;
    public Material previewMaterial;

    public static string[] voxelNames = { "Air", "Stone", "Dirt", "Grass", "Iron Ore", "Gold Ore", "Diamond Ore", "Bedrock",
        "Coal", "Sand", "Wood", "Leaves", "Copper Ore" };

    public static Dictionary<int, float> toolSpeed = new Dictionary<int, float> {
        {5, 0.3f}, {26, 2f}, {28, 1f} //Hand=5f
    };

    public static Dictionary<string, float> fuelTimes = new Dictionary<string, float> {
        {"9:8", 20f}, {"9:10", 10f}, {"9:11", 1f}, {"24", 2f}
    };
    
    void Awake()
    {
        if (resources == null)
            resources = this;
    }

    static Item[] itemBank = new Item[]
    {
        new Item(0, "Ship",             ItemType.None),
        new Item(1, "Block",            ItemType.Block),
        new Item(2, "Hammer",           ItemType.DestroyTool),
        new Item(3, "Control Panel",    ItemType.Block),
        new Item(4, "Pointy Block",     ItemType.Block),
        new Item(5, "Jackhammer",       ItemType.DiggingTool),
        new Item(6, "Engine Box",       ItemType.Block),
        new Item(7, "Window",           ItemType.Block),
        new Item(8, "Door",             ItemType.Block),
        new Item(9, "Voxel",            ItemType.Voxel),
        new Item(10,"Workbench",        ItemType.Block),
        new Item(11,"Furnace",          ItemType.Block),
        new Item(12,"Iron ingot",       ItemType.Component),
        new Item(13,"Iron rod",         ItemType.Component),
        new Item(14,"Iron plate",       ItemType.Component),
        new Item(15,"Gold ingot",       ItemType.Component),
        new Item(16,"Refined diamond",  ItemType.Component),
        new Item(17,"Small Container",  ItemType.Block),
        new Item(18,"Rocket Engine",    ItemType.Block),
        new Item(19,"Reflector",        ItemType.Block),
        new Item(20,"Terminal",         ItemType.Block),
        new Item(21,"Gravity generator",ItemType.Block),
        new Item(22,"Wooden board",     ItemType.Component),
        new Item(23,"Stone anvil",      ItemType.Block),
        new Item(24,"Wooden stick",     ItemType.Component),
        new Item(25,"Rock",             ItemType.Component),
        new Item(26,"Improvised multitool",ItemType.DiggingTool),
        new Item(27,"Glass pane",       ItemType.Component),
        new Item(28,"Iron pickaxe",     ItemType.DiggingTool),
        new Item(29,"Steam engine",     ItemType.Block),
        new Item(30,"Electric motor",   ItemType.Block),
        new Item(31,"Copper ingot",     ItemType.Component),
        new Item(32,"Copper wire",      ItemType.Component),
        new Item(33,"Copper coil",      ItemType.Component),
        new Item(34,"Solar panel",      ItemType.Block),
        new Item(35,"Slope",            ItemType.Block),
        new Item(36,"Slope corner I",   ItemType.Block),
        new Item(37,"Slope corner O",   ItemType.Block),
        new Item(38,"Welding frame",    ItemType.Block),
        new Item(39,"Light",            ItemType.Block),
    };

    public static Item GetItem(int n)
    {
        return itemBank[n].Clone();
    }

    public Planet FindPlanetByType(PlanetType type)
    {
        return planetTypes.First((p) => { return p.type == type; });
    }

    public ExtraDropItem[] GetExtraDropItems(int voxelId)
    {
        return extraDropItems.Where((i) => { return i.voxelId == voxelId; }).ToArray();
    }

}
[System.Serializable()]
public struct ItemData
{
    public GameObject prefab;
    public Sprite sprite;
}

[System.Serializable()]
public struct ExtraDropItem
{
    public int voxelId;
    public float rarity;
    public int itemId;
    public int minCount;
    public int maxCount;
}
