﻿using System.Collections.Generic;
using UnityEngine;

public class Workbench : Block, IActivable
{
    public void Activate(Player player)
    {
        Debug.Log("Workbench activated");
        Player.local.hud.craftingPanel.toolsEnabled = new List<CraftingTool> { CraftingTool.Hand, CraftingTool.Workbench};
        Player.local.input.ShowInventory(true);
    }
}
