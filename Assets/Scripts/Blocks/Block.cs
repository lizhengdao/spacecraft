﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
[RequireComponent(typeof(NetworkChild))]
public class Block : NetworkBehaviour
{
    public int blockID;
    public float Durability = 100;
    public float mass = 1f;
    public bool explosive = false;    

    //Info for placing
    public MountPoints mounts;

    float detachTreshold = 80f;

    public virtual ObjectData SaveBlock()
    {
        //TODO: Save name, durability...
        var data = new ObjectData();
        data.transform = new TransformData(transform);
        data.prefab = blockID.ToString();
        var p = GetComponent<Properties>();
        if(p != null)
            data.data = p.Save();
        else data.data = null;
        return data;
    }

    public virtual void LoadBlock(Dictionary<string,string> data)
    {
        var p = GetComponent<Properties>();
        if(p != null)
            p.Load(data);
    }

    void ProcessCollision(Collision c)
    {
        var damage = c.impulse.magnitude / 5;
        ApplyDamage(damage);
    }

    void ApplyDamage(float damage)
    {
        Durability -= damage;
        print(name + " -" + damage + ": new " + Durability);
        if(Durability < 0)
        {
            if(explosive)
            {
                //TODO: Spawn explosion
            }
            Delete();
        }
        else if(Durability < detachTreshold)
        {
            if(transform.parent.childCount > 1)
                NetworkHelper.current.CmdDetachBlock(gameObject);
                //FIXME: Should be detached in a similar way to CmdDelete (to prevent unconnected ships floating around)
        }
    }

    //Probably could have used delegates or an interface for these since not all blocks need to be updated
    //Should be called on every block when a block is placed or removed
    public virtual void OnUpdateShip()
    {}
    
    [Command]
    public void CmdDelete()
    {
        //Disable collider
        GetComponent<Collider>().enabled = false;
        Transform parentShip = transform.parent;
        //Get "siblings" as list
        List<GameObject> siblings = new List<GameObject>();
        foreach (Transform child in parentShip)
            siblings.Add(child.gameObject);
        //Remove this from siblings
        siblings.Remove(gameObject);
        //while siblings != empty
        List<List<GameObject>> ships = new List<List<GameObject>>();
        while (siblings.Count > 0)
        {
            //create a new ship
            List<GameObject> ship = new List<GameObject>();
            //take first, add it to ship and get neighbors
            var first = siblings[0];
            ship.Add(first);
            siblings.Remove(first);
            var found = first.GetComponent<Block>().FindNeighborsInGroup(siblings);
            ship.AddRange(found);
            siblings = RemoveRange(siblings, found);
            ships.Add(ship);
        }

        //Regroup in new ships
        Debug.Log("new ships: " + ships.Count.ToString());
        if (ships.Count > 1)
        {
            GameObject[] newShips = new GameObject[ships.Count];
            newShips[0] = parentShip.gameObject;
            parentShip.DetachChildren();
            NetworkHelper.current.RpcDetachChildren(parentShip.gameObject);
            var origRb = parentShip.GetComponent<Rigidbody>();
            for (int i = 1; i < ships.Count; i++)
            {
                newShips[i] = Instantiate(parentShip).gameObject;
                NetworkServer.Spawn(newShips[i]);
                //All parts of original ship should continue in motion
                var rb = newShips[i].GetComponent<Rigidbody>();
                rb.velocity = origRb.velocity;
                rb.angularVelocity = origRb.angularVelocity;
            }
            for (int i = 0; i < ships.Count; i++)
            {
                foreach (var block in ships[i])
                {
                    block.transform.SetParent(newShips[i].transform);
                    NetworkHelper.SetNetworkParent(block, newShips[i]);
                }
                newShips[i].GetComponent<Ship>().RpcUpdateShip();
            }
        } else parentShip.GetComponent<Ship>().RpcUpdateShip();
        //Finally destroy the object
        Delete();
    }

    public void Delete()
    {        
        if(transform.parent != null)
        {
            if(transform.parent.childCount < 1)
                NetworkServer.Destroy(transform.parent.gameObject);
        }
        NetworkServer.Destroy(gameObject);
    }

    public GameObject[] FindNeighbors()
    {
        //TODO: Filter out only buildables and dont detect over corners - use raycasting instead of cube
        //FIXME: Use some kind of offset to correctly overlap wall mounted objects (terminal)
        //Cast a cube
        GetComponent<Collider>().enabled = false;
        var colliders = Physics.OverlapBox(transform.position, mounts.halfExtents * 1.05f, transform.rotation);
        var gameObjects = colliders.Select((x) => x.gameObject).ToArray();
        GetComponent<Collider>().enabled = true;
        return gameObjects;
    }

    public GameObject[] FindNeighborsInGroup(List<GameObject> group)
    {
        //foreach if neighbor in siblings
        var neighbors = FindNeighbors();
        List<GameObject> ship = new List<GameObject>();
        foreach (var neighbor in neighbors)
        {
            if (group.Contains(neighbor))
            {
                //add to ship
                ship.Add(neighbor);
                //remove from siblings
                group.Remove(neighbor);
                //Call FindNeighborsInGroup on it
                var found = neighbor.GetComponent<Block>().FindNeighborsInGroup(group);
                ship.AddRange(found);
                group = RemoveRange(group, found);
            }
        }
        return ship.ToArray();
    }

    List<GameObject> RemoveRange(List<GameObject> input, GameObject[] range)
    {
        foreach (var item in range)
            input.Remove(item);
        return input;
    }
}
    