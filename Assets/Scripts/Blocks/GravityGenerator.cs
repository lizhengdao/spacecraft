using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnergyComponent))]
public class GravityGenerator : MonoBehaviour, IElectricAppliance
{
    public int maxForce = 200;
    public int force = 10;
    public int maxRange = 50;
    public int range = 25;
    public float powerConsumption = 1000;

    Properties properties;
    EnergyComponent energyComponent;
    bool isOn = false;

    void Awake()
    {
        World.gravityGenerators.Add(this);
        properties = GetComponent<Properties>();
        properties.Add(new BlockProp("Range", PropType.Int, range.ToString(), maxRange));
        properties.Add(new BlockProp("Force", PropType.Int, force.ToString(), maxForce));
        energyComponent = GetComponent<EnergyComponent>();
        energyComponent.type = EnergyComponentType.Consumer;
        energyComponent.inputNeed = powerConsumption;
    }

    void OnDestroy()
    {
        World.gravityGenerators.Remove(this);
    }

    void FixedUpdate()
    {
        if (!isOn)
            return;
        foreach(var body in World.bodiesWithGravity)
        {
            if ((body.transform == transform.root) || (body.rb == null))
                continue;
            
            float distance = Vector3.Distance(transform.position, body.rb.position);
            if (distance > properties.GetInt("Range"))
                continue;
            
            var locPos = transform.InverseTransformPoint(body.rb.position);
            if (locPos.y < 0)
                continue;
            
            var f = -transform.up * properties.GetInt("Force");
            body.rb.AddForce(f);
        }
    }

    public void OnPoweredOn()
    {
        isOn = true;
    }

    public void OnPoweredOff()
    {
        isOn = false;
    }
}