﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketEngine : MonoBehaviour
{
    [SerializeField()]
    private float power = 100f;

    private Ship ship;
    void Start()
    {
        //Register with ship
        ship = transform.root.GetComponent<Ship>();
        if (ship != null)
            ship.RegisterEngine(transform.localRotation * Vector3.forward, power);
    }

    void OnDisable()
    {
        //Unregister
        if (ship != null)
            ship.UnregisterEngine(transform.localRotation * Vector3.forward, power);
    }
}
