﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activable : MonoBehaviour, IActivable
{
    public virtual void Activate(Player player)
    {
        Debug.Log("Object " + transform.name + " activated");
    }
}

public interface IActivable
{
    void Activate(Player player);
}
