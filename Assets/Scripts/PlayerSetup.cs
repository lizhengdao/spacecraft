﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour {

    public Behaviour[] componentsToDisable;
    public GameObject[] objectsToDisable;

	void Start () {
        if (!isLocalPlayer)
        {
            foreach (var c in componentsToDisable)
            {
                c.enabled = false;
            }
            foreach (var o in objectsToDisable)
            {
                o.SetActive(false);
            }
        }
	}
}
