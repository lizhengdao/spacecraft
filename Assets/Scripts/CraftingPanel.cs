﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingPanel : MonoBehaviour
{
    public Transform recipeListUI;
    public Transform materialListUI;
    public Slot productSlot;
    public GameObject itemButton;

    internal List<CraftingTool> toolsEnabled = new List<CraftingTool> { CraftingTool.Hand };

    CraftingRecipe currentRecipe;
    List<Item> materials = new List<Item>();
    // Use this for initialization
    void Start ()
    {
        productSlot.sourceSlot = true;
        productSlot.grabItem = grabItem;
    }

    public void InitRecipes()
    {
        foreach (Transform obj in recipeListUI)
            Destroy(obj.gameObject);
        
        foreach (var recipe in Crafting.recipes)
        {
            if (toolsEnabled.Contains(recipe.tool))
            {
                var obj = Instantiate(itemButton, recipeListUI);
                var btn = obj.GetComponent<Button>();
                btn.onClick.AddListener(()=>{SetRecipe(recipe);});
                btn.transform.GetChild(0).GetComponent<Image>().sprite = ResourceBank.GetItem(recipe.product).GetSprite();
            }
        }
    }

    void SetRecipe(CraftingRecipe recipe)
    {
        currentRecipe = recipe;
        materials.Clear();
        foreach (Transform child in materialListUI)
            Destroy(child.gameObject);
        string[] s_materials = recipe.materials.Split(';');

        foreach (string m in s_materials)
        {
            //Interpret materials string
            var item = Item.Deserialize(m);
            var obj = Instantiate(ResourceBank.resources.button, materialListUI);
            var btn = obj.GetComponent<Button>();
            btn.GetComponentInChildren<Text>().text = item.stackCount + "x " + item.GetName();
            materials.Add(item);
        }
        productSlot.SetItem(ResourceBank.GetItem(recipe.product));
    }

    bool checkMaterials()
    {
        int availMaterials = 0;
        foreach (var item in materials)
        {
            var count = Player.local.inventory.inventory.CountItem(item) + Player.local.toolbar.inventory.CountItem(item);
            if (count >= item.stackCount)
                availMaterials++;
        }
        return availMaterials == materials.Count;            
    }

    bool grabItem()
    {
        if (checkMaterials())
        {
            foreach (var item in materials)
            {
                int needed = item.stackCount;
                needed -= Player.local.inventory.inventory.CountItem(item);
                Player.local.inventory.inventory.RemoveItem(item);
                if (needed > 0)
                {
                    var itmRest = item.Clone();
                    itmRest.stackCount = needed;
                    Player.local.toolbar.inventory.RemoveItem(itmRest);
                }
            }
            Player.local.inventory.UpdateUI();
            Player.local.toolbar.UpdateUI();
            return true;
        }
        return false;
    }
}
