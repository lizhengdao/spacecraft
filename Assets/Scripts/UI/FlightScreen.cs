﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlightScreen : MonoBehaviour
{
    public Text positionText;
    public Text velocityText;
    public Text speedText;
    public Image needle;
    public Toggle hoverMode;
    public Slider throttleSlider;
    public Slider energySlider;
    public float maxSpeed = 400;
    float nextUpdateTime = 0f;
    float updateRate = 0.5f;
    
    // Update is called once per frame
    void Update ()
    {
        if (Time.time > nextUpdateTime)
        {
            var ship = transform.root.GetComponent<Ship>();

            //Position
            string posText = "X: " + transform.root.position.x.ToString("F3") + "\n";
            posText += "Y: " + transform.root.position.y.ToString("F3") + "\n";
            posText += "Z: " + transform.root.position.z.ToString("F3");
            positionText.text = posText;
            //Velocity
            string velText = "X: " + transform.root.GetComponent<Rigidbody>().velocity.x.ToString("F3")  + "\n";
            velText += "Y: " + transform.root.GetComponent<Rigidbody>().velocity.y.ToString("F3") + "\n";
            velText += "Z: " + transform.root.GetComponent<Rigidbody>().velocity.z.ToString("F3");
            velocityText.text = velText;
            //Speed
            var speed = transform.root.GetComponent<Rigidbody>().velocity.magnitude;
            float angle = speed / maxSpeed * 180;
            if (angle > 180)
                angle = 180;
            needle.transform.localRotation = Quaternion.Euler(0,0,-angle);
            speedText.text = speed.ToString("F2") + " u/s";
            //Throttle
            throttleSlider.value = ship.throttle;
            //Fuel
            //EnergyLoad
            if(ship.energyProduction == 0f) energySlider.value = 0f;
            else energySlider.value = ship.energyConsumption / ship.energyProduction;

            nextUpdateTime = Time.time + updateRate;
        }
    }

    public void ChangeScreen()
    {
        transform.parent.GetComponent<ScreenSwitcher>().SwitchScreen(0);
    }

    public void SetHoverMode()
    {
        //TODO: Only allow hovermode when moving slowly
        transform.root.GetComponent<Ship>().SetHoverMode(hoverMode.isOn);
    }
}
