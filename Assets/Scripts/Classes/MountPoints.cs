﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class MountPoints
{
    public List<Vector3>[] mountPoints = new List<Vector3>[6];
    public static Vector3[] directions = { Vector3.up, Vector3.down, Vector3.back, Vector3.forward, Vector3.left, Vector3.right };

    public Vector3 halfExtents = new Vector3(0.5f, 0.5f, 0.5f); //Default values for cube
    public Sides defaultSide = Sides.Bottom;

    private int sideNum = 0;
    private int mountNum = 0; 

    public MountPoints()
    {
        for (int i = 0; i < 6; i++)
        {
            mountPoints[i] = new List<Vector3>();
        }
        //Calculate mount points
        Calculate();
    }

    public void Calculate()
    {
        sideNum = (int)defaultSide;
        for (int i = 0; i < 6; i++)
        {
            mountPoints[i].Clear();
        }
        //Top and bottom
        for (float x = -halfExtents.x + 0.5f; x <= halfExtents.x; x += 1f)
        {
            for (float z = -halfExtents.z + 0.5f; z <= halfExtents.z; z += 1f)
            {
                mountPoints[(int)Sides.Top].Add(new Vector3(x, halfExtents.y, z));
                mountPoints[(int)Sides.Bottom].Add(new Vector3(x, -halfExtents.y, z));
            }
        }
        //Sides
        for (float y = -halfExtents.y + 0.5f; y <= halfExtents.y; y += 1f)
        {
            //Left and right
            for (float z = -halfExtents.z + 0.5f; z <= halfExtents.z; z += 1f)
            {
                mountPoints[(int)Sides.Right].Add(new Vector3(halfExtents.x, y, z));
                mountPoints[(int)Sides.Left].Add(new Vector3(-halfExtents.x, y, z));
            }
            //Front and back
            for (float x = -halfExtents.x + 0.5f; x <= halfExtents.x; x += 1f)
            {
                mountPoints[(int)Sides.Front].Add(new Vector3(x, y, halfExtents.z));
                mountPoints[(int)Sides.Back].Add(new Vector3(x, y, -halfExtents.z));
            }
        }
    }

    public Vector3 GetDefault()
    {
        return mountPoints[(int)defaultSide][0];
    }

    public Vector3 GetCurrent(out Vector3 dir)
    {
        if (mountNum >= mountPoints[sideNum].Count)
            mountNum = 0;
        dir = directions[sideNum];
        return mountPoints[sideNum][mountNum];
    }

    public void SetDirection(Sides side)
    {
        sideNum = (int)side;
        mountNum = 0;
    }

    public void Next()
    {
        mountNum++;
        if (mountNum >= mountPoints[sideNum].Count)
            mountNum = 0;
    }

    public void Rotate(Vector3 angles)
    {
        var vect = directions[sideNum];
        vect = Quaternion.Euler(angles) * vect;
        for (int i = 0; i < 6; i++)
        {
            if (vect == directions[i])
                sideNum = i;
        }
        mountNum = 0;
    }
}
