﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class World
{
    public static List<PlanetObject> planets = new List<PlanetObject>();
    public static List<PlanetChunk> chunks = new List<PlanetChunk>();
    public static List<Gravity> bodiesWithGravity = new List<Gravity>();

    public static List<GravityGenerator> gravityGenerators = new List<GravityGenerator>();

    // TODO: LOD levels should change depending on graphics settings
    public static int[] LODSteps = { 60, 100, 200 };
    public static float minImpactVelocity = 7f;
    
    public static PlanetObject FindClosestPlanet(Vector3 pos)
    {
        if (planets.Count < 1) 
            return null;
        return planets.Aggregate((min, next) =>
            Vector3.Distance(min.transform.position, pos) < 
            Vector3.Distance(next.transform.position, pos) ?
            min : next);
    }

    public static GravityGenerator FindClosestGravityGenerator(Vector3 pos)
    {
        GravityGenerator best = null;
        float bestDist = float.PositiveInfinity;
        for(int i = 0; i < gravityGenerators.Count; i++)
        {
            var dist = Vector3.Distance(gravityGenerators[i].transform.position, pos);
            if((dist < bestDist) && (dist < gravityGenerators[i].range))
            {
                best = gravityGenerators[i];
                bestDist = dist;
            }
        }
        return best;
    }

    public static PlanetChunk FindChunkWithID(string ID)
    {
        return chunks.FirstOrDefault(x => x.ID == ID);
    }

    public static PlanetChunk FindChunkAtPosition(Vector3 pos)
    {
        return chunks.FirstOrDefault(x => x.transform.position.IsClose(pos));
    }
}
