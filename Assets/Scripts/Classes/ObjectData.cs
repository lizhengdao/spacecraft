﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable()]
public class ObjectData {

    public TransformData transform;
    public string prefab;
    public Dictionary<string, string> data;

    public byte[] Serialize()
    {
        BinaryFormatter bf = new BinaryFormatter();  
        var ms = new System.IO.MemoryStream();  
        bf.Serialize(ms, this);

        return ms.ToArray();
    }
}
